// In App.js in a new project

import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './src/screens/home/HomeScreen';
import CalendarScreen from './src/screens/calendar/CalendarScreen';
import Post from './src/screens/post/Post';
import Stats from './src/screens/stats/Stats';
import Settings from './src/screens/settings/Settings';
import postList from './src/screens/postlist/postList';
import editPost from './src/screens/editpost/editPost';
import DataBase from './src/utils/database';
import './src/utils/i18n';
import {useTranslation} from 'react-i18next';
import BottomMenu from './src/UI/bottomMenu/BottomMenu';
import {SafeAreaView, View} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';

const db = DataBase.getConnection();

const Stack = createStackNavigator();

function App() {
  const {t, i18n} = useTranslation();

  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql(
        'create table if not exists items (id integer primary key not null, date text, emo int, level int, descr text, trigger text);',
      );
    });
  }, []);

  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="BottomMenu"
            options={{headerShown: false}}
            component={BottomMenu}
          />
          {/*<Stack.Screen*/}
          {/*  name="Edit"*/}
          {/*  component={editPost}*/}
          {/*  options={{title: t('Edit')}}*/}
          {/*/>*/}
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}

export default App;
