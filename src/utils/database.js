import * as React from 'react';
import * as SQLite from 'expo-sqlite';

const conn = SQLite.openDatabase('ed.db');

class DataBase {
    getConnection() {
        return conn;
    }
}

module.exports = new DataBase();
