import {createStackNavigator} from '@react-navigation/stack';
import CalendarScreen from '../screens/calendar/CalendarScreen';
import postList from '../screens/postlist/postList';
import editPost from '../screens/editpost/editPost';
import Post from '../screens/post/Post';
import React from 'react';
import {useTranslation} from 'react-i18next';

const CalendarStack = createStackNavigator();

export default function CalendarNavigator() {
  const {t, i18n} = useTranslation();

  return (
    <CalendarStack.Navigator>
      <CalendarStack.Screen
        name="Calendar"
        component={CalendarScreen}
        options={{title: t('Calendar'), headerShown: false}}
      />
      <CalendarStack.Screen
        name="List"
        component={postList}
        options={{title: t('List')}}
      />
      <CalendarStack.Screen
        name="Edit"
        component={editPost}
        options={{title: t('Edit')}}
      />
      <CalendarStack.Screen
        name="Post"
        component={Post}
        options={{title: t('Post')}}
      />
    </CalendarStack.Navigator>
  );
}
