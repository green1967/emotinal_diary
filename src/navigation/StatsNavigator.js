import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Stats from '../screens/stats/Stats';

const StatsStack = createStackNavigator();

export default function StatsNavigator() {
  return (
    <StatsStack.Navigator>
      <StatsStack.Screen
        name="Stats"
        component={Stats}
        options={{headerShown: false}}
      />
    </StatsStack.Navigator>
  );
}
