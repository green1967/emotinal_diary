import * as React from 'react';
import {Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {MaterialCommunityIcons} from '@expo/vector-icons';
import HomeScreen from '../../screens/home/HomeScreen';
import CalendarScreen from '../../screens/calendar/CalendarScreen';
import Post from '../../screens/post/Post';
import Stats from '../../screens/stats/Stats';
import Settings from '../../screens/settings/Settings';
import CalendarNavigator from '../../navigation/CalendarNavigator';
import StatsNavigator from '../../navigation/StatsNavigator';
import postList from '../../screens/postlist/postList';
import editPost from '../../screens/editpost/editPost';
import DataBase from '../../utils/database';

const Tab = createMaterialBottomTabNavigator();

function BottomMenu() {
  return (
    <Tab.Navigator
      initialRouteName="HomeScreen"
      activeColor="#e91e63"
      labelStyle={{fontSize: 12}}
      style={{backgroundColor: 'tomato'}}>
      <Tab.Screen
        name="Post"
        component={Post}
        options={{
          tabBarLabel: 'post',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="plus" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Calendar"
        component={CalendarNavigator}
        options={{
          tabBarLabel: 'Calendar',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="calendar" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Stats"
        component={StatsNavigator}
        options={{
          tabBarLabel: 'Statistics',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="chart-arc" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Settings"
        component={Settings}
        options={{
          tabBarLabel: 'Settings',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="account-settings"
              color={color}
              size={26}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default BottomMenu;
