import React, {useEffect, useState} from 'react';
import { View, Text , StyleSheet} from 'react-native';
import { Button } from 'react-native-elements';

config = require('../../config.json');

export default ShowList = (props) => {
    let show = [];

        props.list.forEach(item=>{
            let emoName='';
//        console.log('item=', item);
        config.emo.forEach(itm => {if(item.emo == itm.value) emoName=itm.label})
        show.push(<View key={item.id}>
            <Text style={styles.item}>{item.time}   {emoName}  {item.descr}</Text>
            <Button title="Edit" onPress={()=>props.onClick(item.id)}/>
            </View>)}
        );

    return (
        <View style={styles.container}>
            {show}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
     flex: 1,
     paddingTop: 22
    },
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
  })