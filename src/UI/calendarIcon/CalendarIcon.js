import * as React from "react"
import Svg, { Rect } from "react-native-svg"

function CalendarIcon(props) {
  return (
    <Svg width={28} height={28} viewBox="0 0 28 28" fill="none" {...props}>
      <Rect
        x={7}
        y={11}
        width={14}
        height={10}
        rx={1}
        stroke={props.color}
        strokeWidth={2}
      />
      <Rect x={6.5} y={6.5} width={15} height={1} rx={0.5} stroke={props.color} />
      <Rect
        x={3}
        y={3}
        width={22}
        height={22}
        rx={3}
        stroke={props.color}
        strokeWidth={2}
      />
    </Svg>
  )
}

export default CalendarIcon
