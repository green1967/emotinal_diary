import React from 'react'
import {
    Text,
    View,
    Dimensions,
    StyleSheet,
  } from 'react-native';
import { AreaChart, Grid } from 'react-native-svg-charts'
import { Circle, Path } from 'react-native-svg';
import DataBase from '../../utils/database';
import { Button } from 'react-native-elements';
import { format, addDays, subDays } from 'date-fns';

config = require('../../config.json');

const db = DataBase.getConnection();

class LineChart extends React.PureComponent {
    state = {
        data : [],
    }

    componentDidMount() {
        this.onDayHundle();
    }

    onDayHundle = ()=>{
        const endDate = format(addDays(new Date(), 1), "yyyy-MM-dd");
        const startDate = format(new Date(), "yyyy-MM-dd");
  //       console.log(startDate, endDate);
        let tmpData=[];
        db.transaction(tx => { 
//            config.emo.forEach(item =>{               
            tx.executeSql(
              `select emo, date from items where (date between '`+startDate+`' and '`+endDate+`') ORDER by date;`,
              [],
              (_, { rows: {_array} }) => {
                        _array.forEach( item => {                   
                        let tmpObj = {
                            value: item.emo,
                            x: Date.parse(item.date)
                        }
                        tmpData.push(tmpObj); 
                        })
//                 console.log('value=', item.value, 'result=',_array);                    
              });
//            })
            },
            ()=>{console.log("Erorr!")},
            ()=>{this.setState({ data: tmpData})}
            );
    }

    onWeekHundle = ()=>{
        const endDate = format(addDays(new Date(), 1), "yyyy-MM-dd");
        const startDate = format(subDays(new Date(), 7), "yyyy-MM-dd");
  //       console.log(startDate, endDate);
        let tmpData=[];
        db.transaction(tx => { 
//            config.emo.forEach(item =>{               
            tx.executeSql(
              `select emo, date from items where (date between '`+startDate+`' and '`+endDate+`') ORDER by date;`,
              [],
              (_, { rows: {_array} }) => {
                        _array.forEach( item => {                   
                        let tmpObj = {
                            value: item.emo,
                            x: Date.parse(item.date)
                        }
                        tmpData.push(tmpObj); 
                        })
//                 console.log('value=', item.value, 'result=',_array);                    
              });
//            })
            },
            ()=>{console.log("Erorr!")},
            ()=>{this.setState({ data: tmpData})}
            );
    }

    onMonthHundle = ()=>{
        const endDate = format(addDays(new Date(), 1), "yyyy-MM-dd");
        const startDate = format(subDays(new Date(), 30), "yyyy-MM-dd");
  //       console.log(startDate, endDate);
        let tmpData=[];
        db.transaction(tx => { 
//            config.emo.forEach(item =>{               
            tx.executeSql(
              `select emo, date from items where (date between '`+startDate+`' and '`+endDate+`') ORDER by date;`,
              [],
              (_, { rows: {_array} }) => {
                        _array.forEach( item => {                   
                        let tmpObj = {
                            value: item.emo,
                            x: Date.parse(item.date)
                        }
                        tmpData.push(tmpObj); 
                        })
//                 console.log('value=', item.value, 'result=',_array);                    
              });
//            })
            },
            ()=>{console.log("Erorr!")},
            ()=>{this.setState({ data: tmpData})}
            );
    }

    

    render() {
        let show = <View style={{alignItems: 'center'}}><Text>No data found</Text></View>
        const Decorator = ({ x, y, data }) => {
            return this.state.data.map((item, index) => {
 //               console.log('item=', item);
                return <Circle
                    key={ index }
                    cx={ x(item.x) }
                    cy={ y(item.value) }
                    r={ 4 }
                    stroke={ 'rgb(134, 65, 244)' }
                    fill={ 'white' }
                />
            })
        }

        const Line = ({ line }) => (
            <Path
                d={ line }
                stroke={ 'rgba(134, 65, 244)' }
                fill={ 'none' }
            />
        )
            if(this.state.data.length) show = 
            <AreaChart
                style={{ height: 200 }}
                data={ this.state.data }      
                yAccessor={ ({ item }) => item.value }
                xAccessor={ ({ item }) => item.x }          
                svg={{ fill: 'rgba(134, 65, 244, 0.2)' }}
                contentInset={{ top: 20, bottom: 30 }}
            >
                <Grid/>
                <Line/>
                <Decorator/>
            </AreaChart>
            
        return (
            <>
            <View style = {styles.container}>
                <Button
                title="Day"
                buttonStyle = {[styles.itemWidth, styles.itemMargin]}
                onPress={this.onDayHundle}
                />
                <Button
                title="Week"
                buttonStyle = {[styles.itemWidth, styles.itemMargin]}
                onPress={this.onWeekHundle}
                />
                <Button
                title="Month"
                buttonStyle = {[styles.itemWidth, styles.itemMargin]}
                onPress={this.onMonthHundle}
                />
            </View>
            <View style={{ justifyContent: 'center', flex: 1 }}>
                {show}
            </View>
            </>            
        )
    }

}

const styles = StyleSheet.create({
    container: {
      flex: 1, 
      alignItems: 'center', 
      justifyContent: 'center',
    },
      itemWidth: {
      width: 300,
    },
    itemMargin: {
      margin: 5,
    }
  });

export default LineChart;
