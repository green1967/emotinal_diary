import React, {useState, useEffect} from 'react';
import { TextInput, View, Text, StyleSheet } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import DropDownPicker from 'react-native-dropdown-picker';
import { Button } from 'react-native-elements';
import DataBase from '../../utils/database';


const config = require('../../config.json');

const db = DataBase.getConnection();


export default function editPost({ navigation, route }) {
  
  const {selectedId} = route.params;
  const {last} = route.params;

  
  const [id, setId] = useState(selectedId);
  const [date, setDate] = useState(new Date());
  const [shortDate, setShortDate] = useState('');
  const [shortTime, setShortTime] = useState('');
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [descr, setDescr] = useState('');
  const [trigger, setTrigger] = useState('');
  const [emo, setEmo] = useState(1);
  const [level, setLevel] = useState(2);

  useEffect(() => {
    db.transaction(tx => {
        tx.executeSql(
          `select id, date, date(date) as d, time(date) as t, emo, level, descr, trigger from items where id=?;`,
          [id],
          (_, { rows: {_array} }) => {
            _array.forEach(item => {
                setDate(new Date(item.date));
                setShortDate(item.d);
                setShortTime(item.t);
                setEmo(item.emo);
                setDescr(item.descr);
                setLevel(item.level);
                setTrigger(item.trigger);
           console.log('item=',item);
            })
          });
      }); 
  }, []);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

    
    return (
      <View style={styles.container}>
      <View>
        <Text style={{fontSize: 30}}>New Post</Text>
      </View>
      <View style={{ flexDirection: 'row'}}>
       <Button
          onPress = {showDatepicker}
          buttonStyle = {styles.btnGrey}
          title = {date.toDateString()}
       />
       <Button
          onPress = {showTimepicker}
          buttonStyle = {styles.btnGrey}
          title = {date.toTimeString()}
       />
      </View>
       <View>
       {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
       </View>
       <View>
       <DropDownPicker
          items={config.emo}
          placeholder="Search and select, please, the emotion"
          defaultValue = {emo.toString()}
          searchable={true}
          searchablePlaceholder="Search..."
          searchableError={() => <Text>Not Found</Text>}
          containerStyle={grpStyles.contPicker}
          style={grpStyles.stylePicker}
          dropDownStyle={{backgroundColor: 'white'}}
          onChangeItem={item => setEmo(item.value)}
      />
       </View>
       <View>
       <DropDownPicker
          items={config.level}
          placeholder="Select, please, the emotion level"
          defaultValue = {level.toString()}
          containerStyle={grpStyles.contPicker}
          style={grpStyles.stylePicker}
          dropDownStyle={styles.itemBgColor}
          onChangeItem={item => setLevel(item.value)}
      />
       </View>      
       <View>  
       <TextInput
        multiline
        placeholder="What's up?"
        defaultValue = {descr}
        onChangeText={text => setDescr(text)}
        style={[styles.hBig, styles.itemWidth, styles.itemMargin, styles.itemBgColor]}
      />
        </View>
      <View>
         <TextInput
         placeholder="What's trigger?"
         onChangeText={text => setTrigger(text)}
         defaultValue={trigger}
         style={[styles.itemWidth, styles.itemBgColor, styles.itemMargin]}
         />
       </View>
        <Button
          title="Save"
          buttonStyle = {[styles.itemWidth, styles.itemMargin]}
          onPress={() => db.transaction(
            tx => {
              tx.executeSql("update  items set date=?, emo=?, level=?, descr=?, trigger=? where id=?", [date.toISOString(),emo,level,descr,trigger,id]);
              tx.executeSql("select * from items", [], (_, { rows }) =>
                console.log(JSON.stringify(rows))
              );
            },
            null,
            navigation.navigate('List', {item: {id: id, date:shortDate, time:shortTime, emo: emo, level: level, descr: descr, trigger: trigger }})
          )}
        />
        <Button
          title="Delete"
          buttonStyle = {[styles.itemWidth, styles.itemMargin]}
          onPress={() => db.transaction(
            tx => {
              tx.executeSql("delete from items where id=?", [id]);
              // tx.executeSql("select * from items", [], (_, { rows }) =>
              //   console.log(JSON.stringify(rows))
              // );
            },
            null,
            navigation.navigate(last?'Calendar':'List', {item: {}, refresh: Math.random()})
          )}
        />
      </View>
      
    );
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1, 
      alignItems: 'center', 
      justifyContent: 'center',
    },
      itemWidth: {
      width: 300,
    },
    itemBgColor: {
      backgroundColor: 'white',
    },
    itemMargin: {
      margin: 5,
    },
    btnGrey: {
      backgroundColor: 'lightgrey',
    },
    hNorm: {
      height: 40,
    },
    hBig: {
      height: 200,
    },    
  });

  const grpStyles = StyleSheet.create({
    contPicker: {
      height: styles.hNorm.height,
      margin: styles.itemMargin.margin,
    },
    stylePicker: {
      width: styles.itemWidth.width,
      backgroundColor: styles.itemBgColor.backgroundColor,
    }
  });