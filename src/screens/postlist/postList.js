import React, {useEffect, useState} from 'react';
import DataBase from '../../utils/database';
import ShowList from '../../UI/showList/showList';
import { format, addDays} from 'date-fns';

const db = DataBase.getConnection();


export default function postList({ navigation, route }) {

    const [pList, setpList] = useState([]);

 
    useEffect(() => {        
            const {selectedDate} = route.params; 
            console.log(selectedDate.day);    
            let startDate = selectedDate.dateString;
            let endDate = format(addDays(new Date(selectedDate.dateString), 1), "yyyy-MM-dd");
//            console.log(startDate, endDate);
            db.transaction(tx => {                
                tx.executeSql(
                  `select id, date, date(date) as d, time(date) as t, emo, level, descr, trigger from items where date between '`+startDate+`' and '`+endDate+`';`,
                  [],
                  (_, { rows: {_array} }) => {
                    setpList(_array);
  //                  console.log(_array);                    
                  });
                });

  
//            console.log('pList=', pList);
        // }
//        console.log(pList.length);
           
      }, [route.params?.item]);
    
  //useEffect(()=> {if(pList.length == 0) navigation.navigate('Calendar');}, [pList]);            

    const onClickHandle = (id) => {
        console.log(id);
        let theLast = pList.length>1?false:true;
 //       console.log('last=', theLast);
        navigation.navigate('Edit', {selectedId: id, last: theLast});
    }
    
    return (      
        <ShowList list={pList}  onClick={onClickHandle}/>
    )

}

