import React from 'react';
import {
    Text,
    View,
    Dimensions,
    StyleSheet,
  } from 'react-native';
import { PieChart } from 'react-native-svg-charts';
import DataBase from '../../utils/database';
import { Button } from 'react-native-elements';
import { format, addDays, subDays } from 'date-fns';

config = require('../../config.json');

const db = DataBase.getConnection();
let countData = [];
let keys = [];
let values = [];

class PieChartFig extends React.PureComponent {
    state = {
        selectedSlice: {
            label: 'No data found',
            index: ''
          },
        labelWidth: 0
    }


    componentDidMount() {
 //      keys.length=0; values.length=0;
        this.onDayHandle();
        // config.emo.forEach((item) => {
        //     keys.push(item.label);
        //     console.log('item.label=', item.label, keys.length);
        // })        
//        this.setState({ selectedSlice: { label: keys[0], value: values[0] } }, ()=>{console.log('key[0]=', keys[0], 'value[0]=', values[0])});
        // let selectedSlice = {
        //     label: values[0],
        //     value: keys[0]
        // }
        // console.log('selectedSlice=', selectedSlice, 'key=', keys[0]);
        // this.setState({selectedSlice: selectedSlice}, ()=>{console.log("state.data=",this.state)});
    }


    onDayHandle = () => {
        const startDate = format(new Date(), "yyyy-MM-dd");
        const endDate = format(addDays(new Date(), 1), "yyyy-MM-dd");
        keys.length=0; values.length=0;
        console.log(startDate, endDate);
        
        db.transaction(tx => { 
            config.emo.forEach(item =>{               
            tx.executeSql(
              `select count(*) as c from items where (date between '`+startDate+`' and '`+endDate+`') and emo=?;`,
              [item.value],
              (_, { rows: {_array} }) => {
                    if(_array[0].c) {
                        if(_array[0].c) {
                        keys.push(item.label);
                        values.push(_array[0].c);
                        }
                    }
 //                 console.log('value=', item.value, 'result=',_array);                    
              });
            })
            },
            ()=>{console.log("Error!")},
            ()=>{keys.length?this.setState({ selectedSlice: { label: keys[0], value: values[0]}}):this.setState({ selectedSlice: { label: "No data found'", value: ""}})}
            );
        
    }

    onWeekHandle = () => {
        const endDate = format(addDays(new Date(), 1), "yyyy-MM-dd");
        const startDate = format(subDays(new Date(), 7), "yyyy-MM-dd");
        keys.length=0; values.length=0;
 //       console.log(startDate, endDate);
        
        db.transaction(tx => { 
            config.emo.forEach(item =>{               
            tx.executeSql(
              `select count(*) as c from items where (date between '`+startDate+`' and '`+endDate+`') and emo=?;`,
              [item.value],
              (_, { rows: {_array} }) => {
                    if(_array[0].c) {
                        if(_array[0].c) {
                        keys.push(item.label);
                        values.push(_array[0].c);
                        }
                    }
 //                 console.log('value=', item.value, 'result=',_array);                    
              });
            })
            },
            ()=>{console.log("Erorr!")},
            ()=>{keys.length?this.setState({ selectedSlice: { label: keys[0], value: values[0]}}):this.setState({ selectedSlice: { label: "No data found'", value: ""}})}
            );
        
    }

    onMonthHandle = () => {
        const endDate = format(addDays(new Date(), 1), "yyyy-MM-dd");
        const startDate = format(subDays(new Date(), 30), "yyyy-MM-dd");
        keys.length=0; values.length=0;
        console.log(startDate, endDate);
        
        db.transaction(tx => { 
            config.emo.forEach(item =>{               
            tx.executeSql(
              `select count(*) as c from items where (date between '`+startDate+`' and '`+endDate+`') and emo=?;`,
              [item.value],
              (_, { rows: {_array} }) => {
                    if(_array[0].c) {
                        if(_array[0].c) {
                        keys.push(item.label);
                        values.push(_array[0].c);
                        }
                    }
 //                 console.log('value=', item.value, 'result=',_array);                    
              });
            })
            },
            ()=>{console.log("Erorr!")},
            ()=>{keys.length?this.setState({ selectedSlice: { label: keys[0], value: values[0]}}):this.setState({ selectedSlice: { label: "No data found'", value: ""}})}
            );
        
    }

    
    render() {

        const { labelWidth, selectedSlice } = this.state;
        const { label, value } = selectedSlice;
//        const keys = ['google', 'facebook', 'linkedin', 'youtube', 'Twitter'];
 //       const values = [15, 25, 35, 45, 55];
        const colors = ['#600080', '#9900cc', '#c61aff', '#d966ff', '#ecb3ff']
        const data = keys.map((key, index) => {
 //           console.log('keys=', key, 'value=', values[index], 'index=', index)
            return {
              key,
              value: values[index],
              svg: { fill: colors[index] },
              arc: { outerRadius: (70 + values[index]) + '%', padAngle: label === key ? 0.1 : 0 },
              onPress: () => this.setState({ selectedSlice: { label: key, value: values[index] } })
            }
          })
        const deviceWidth = Dimensions.get('window').width
 
        return (
            <>
            <View style = {styles.container}>
                <Button
                title="Day"
                buttonStyle = {[styles.itemWidth, styles.itemMargin]}
                onPress={this.onDayHandle}
                />
                <Button
                title="Week"
                buttonStyle = {[styles.itemWidth, styles.itemMargin]}
                onPress={this.onWeekHandle}
                />
                <Button
                title="Month"
                buttonStyle = {[styles.itemWidth, styles.itemMargin]}
                onPress={this.onMonthHandle}
                />
            </View>
            <View style={{ justifyContent: 'center', flex: 1 }}>
              <PieChart
                style={{ height: 200 }}
                outerRadius={'80%'}
                innerRadius={'45%'}
                data={data}
              />
              <Text
                onLayout={({ nativeEvent: { layout: { width } } }) => {
                  this.setState({ labelWidth: width });
                }}
                style={{
                  position: 'absolute',
                  left: deviceWidth / 2 - labelWidth / 2,
                  textAlign: 'center'
                }}>
                {`${label} \n ${value}`}
              </Text>
            </View>
            </>
          )
    }
    }

    const styles = StyleSheet.create({
        container: {
          flex: 1, 
          alignItems: 'center', 
          justifyContent: 'center',
        },
          itemWidth: {
          width: 300,
        },
        itemMargin: {
          margin: 5,
        }
      });

export default PieChartFig;