/* eslint-disable react/prop-types */
import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import {useTranslation} from 'react-i18next';
import {createStackNavigator} from '@react-navigation/stack';
import CalendarScreen from '../calendar/CalendarScreen';
import Post from '../post/Post';
import Stats from '../stats/Stats';
import Settings from '../settings/Settings';
import postList from '../postlist/postList';
import editPost from '../editpost/editPost';

// eslint-disable-next-line no-unused-vars
export default function HomeScreen({navigation, route}) {
  // useEffect(() => {
  //   if (route.params?.post) {
  //     // Post updated, do something with `route.params.post`
  //     // For example, send the post to the server
  //   }
  // }, [route.params?.post]);

  const {t, i18n} = useTranslation();

  const Stack = createStackNavigator();

  return (
    <Stack.Navigator initialRouteName="Post">
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{title: t('Emotional Diary')}}
      />
      <Stack.Screen
        name="Calendar"
        component={CalendarScreen}
        options={{title: t('Calendar')}}
      />
      <Stack.Screen
        name="List"
        component={postList}
        options={{title: t('List')}}
      />
      <Stack.Screen
        name="Edit"
        component={editPost}
        options={{title: t('Edit')}}
      />
      <Stack.Screen name="Post" component={Post} options={{title: t('Post')}} />
      <Stack.Screen
        name="Stats"
        component={Stats}
        options={{title: t('Statistics')}}
      />
      <Stack.Screen
        name="Settings"
        component={Settings}
        options={{title: t('Settings')}}
      />
    </Stack.Navigator>

    // <View style={styles.container}>
    //    <Button
    //     title={t('Create post')}
    //     buttonStyle = {styles.btn}
    //     onPress={() => navigation.navigate('Post')}
    //   />
    //   <Button
    //     title={t('Calendar')}
    //     buttonStyle = {styles.btn}
    //     onPress={() => navigation.navigate('Calendar')}
    //   />
    //   <Button
    //     title={t('Statistics')}
    //     buttonStyle = {styles.btn}
    //     onPress={() => navigation.navigate('Stats')}
    //   />
    //   <Button
    //     title={t('Settings')}
    //     buttonStyle = {styles.btn}
    //     onPress={() => navigation.navigate('Settings')}
    //   />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    width: 300,
    margin: 5,
  },
});
