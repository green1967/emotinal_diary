import React, {useState, useEffect} from 'react';
import {TextInput, View, Text, StyleSheet, TouchableHighlight} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import DropDownPicker from 'react-native-dropdown-picker';
import {Button} from 'react-native-elements';
import DataBase from '../../utils/database';
import * as emoList from '../../config.json';
import CalendarIcon from '../../UI/calendarIcon/CalendarIcon';
import ClockIcon from '../../UI/clockIcon/CloclIcon';

// config = require('../../config.json');

const db = DataBase.getConnection();
const DEF_LEVEL = 2;

export default function Post({navigation, route}) {
  var curTime;
  var newDate = new Date();
  var back2Calendar;
  if (route.params?.selectedDate) back2Calendar = true;
  else back2Calendar = false;
  // route.params?.refresh?refresh=false:refresh=true;
  console.log(`back2calendar=${back2Calendar} routeParams=${route.params}`);

  const [date, setDate] = useState(newDate);
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [descr, setDescr] = useState('test description');
  const [trigger, setTrigger] = useState('test trigger');
  const [emo, setEmo] = useState(0);
  const [level, setLevel] = useState(DEF_LEVEL);
  const [selectedItem, setSelectedItem] = useState(0);

  useEffect(() => {
    if (route.params?.selectedDate) {
      const {selectedDate} = route.params;
      let now = new Date();
      curTime = now.toISOString().split('T');
      newDate = new Date(selectedDate + 'T' + curTime[1]);
    } else newDate = new Date();
    setDate(newDate);
  }, [route.params?.selectedDate]);

  //  console.log(`${emoList.level}`);
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
    setSelectedItem(1);
  };

  const showTimepicker = () => {
    showMode('time');
    setSelectedItem(2);
  };

  const onPressHandle = () => {
    console.log('date=', date);
    db.transaction(
      (tx) => {
        tx.executeSql(
          'insert into items (date, emo, level, descr, trigger) values (?, ?, ?, ?, ?)',
          [date.toISOString(), emo, level, descr, trigger],
          (_, {insertId}) => {
            console.log('insertID=', insertId);
          },
          (_, error) => console.log('ERROR', error),
        );
        // tx.executeSql("select * from items", [], (_, { rows }) =>
        //   console.log(JSON.stringify(rows))
        // );
        //        console.log("INSIDE");
      },
      null,
      () => {
        route.params?.fetchData();
        navigation.goBack();
      },
    );
    // console.log('date=', date);
  };

  return (
    <View style={styles.container}>
      <View>
        <Text style={{fontSize: 30}}>New Post</Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        <View style={{flexDirection: 'row', borderStyle: "solid", borderColor: "red", borderWidth: selectedItem==1?1:0, marginRight: 10}}>
        <View style={{padding: 7 }}>
          <Text onPress={showDatepicker}>
            {date.toDateString()}
          </Text>
        </View>
          <View style={{padding: 4 }}>
            <CalendarIcon onPress={showDatepicker} color={selectedItem==1?"#CF6956":"#4F4F4F"}/>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderStyle: "solid", borderColor: "red", borderWidth: selectedItem==2?1:0}}>
        <View style={{padding: 7 }}>
          <Text onPress={showTimepicker}>
            {(date.toTimeString().split(' '))[0]}
          </Text>
        </View>
          <View style={{padding: 4 }}>
            <ClockIcon onPress={showTimepicker} color={selectedItem==2?"#CF6956":"#4F4F4F"}/>
          </View>
        </View>
      </View>
      <View>
        {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode={mode}
            is24Hour
            display="default"
            onChange={onChange}
          />
        )}
      </View>
      <View>
        <DropDownPicker
          items={emoList.emo}
          placeholder="Search and select, please, an emotion"
          searchable
          searchablePlaceholder="Search..."
          searchableError={() => <Text>Not Found</Text>}
          containerStyle={grpStyles.contPicker}
          style={grpStyles.stylePicker}
          dropDownStyle={{backgroundColor: 'white'}}
          onChangeItem={(item) => setEmo(item.value)}
        />
      </View>
      <View>
        <DropDownPicker
          items={emoList.level}
          placeholder="Select, please, the emotion level"
          containerStyle={grpStyles.contPicker}
          style={grpStyles.stylePicker}
          dropDownStyle={styles.itemBgColor}
          onChangeItem={(item) => setLevel(item.value)}
        />
      </View>
      <View>
        <TextInput
          multiline
          placeholder="What's WHAAAAATTTTTTT?"
          onChangeText={(text) => setDescr(text)}
          style={[
            styles.hBig,
            styles.itemWidth,
            styles.itemMargin,
            styles.itemBgColor,
          ]}
        />
      </View>
      <View>
        <TextInput
          placeholder="What's trigger?"
          onChangeText={(text) => setTrigger(text)}
          style={[styles.itemWidth, styles.itemBgColor, styles.itemMargin]}
        />
      </View>
      <Button
        title="Save"
        buttonStyle={[styles.itemWidth, styles.itemMargin]}
        onPress={onPressHandle}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemWidth: {
    width: 300,
  },
  itemBgColor: {
    backgroundColor: 'white',
  },
  itemMargin: {
    margin: 5,
  },
  btnGrey: {
    backgroundColor: 'lightgrey',
  },
  hNorm: {
    height: 40,
  },
  hBig: {
    height: 200,
  },
});

const grpStyles = StyleSheet.create({
  contPicker: {
    height: styles.hNorm.height,
    margin: styles.itemMargin.margin,
  },
  stylePicker: {
    width: styles.itemWidth.width,
    backgroundColor: styles.itemBgColor.backgroundColor,
  },
});
