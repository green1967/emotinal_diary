/* eslint-disable react/prop-types */
import React, {useState, useEffect, useCallback} from 'react';
import {StyleSheet, Text} from 'react-native';
import {Calendar} from 'react-native-calendars';
import DataBase from '../../utils/database';
import {format, startOfMonth, addMonths} from 'date-fns';
import {DefaultTheme} from '@react-navigation/native';
import {SafeAreaView} from 'react-native-safe-area-context';

const db = DataBase.getConnection();

export default function CalendatScreen({navigation, route}) {
  const [items, setItems] = useState(null);
  const [markedItems, setMarkedItems] = useState(null);
  const [curDate, setCurDate] = useState(format(new Date(), 'yyyy-MM-dd'));

  const fetchData = useCallback(() => {
    let startDate = format(startOfMonth(new Date(curDate)), 'yyyy-MM-dd');
    let endDate = format(
      startOfMonth(addMonths(new Date(curDate), 1)),
      'yyyy-MM-dd',
    );
    //         console.log(startDate, endDate);
    db.transaction(
      (tx) => {
        tx.executeSql(
          `select id, date(date) as date, time(date) as time, emo, level, descr, trigger from items where date between '` +
            startDate +
            `' and '` +
            endDate +
            `';`,
          [],
          (_, {rows: {_array}}) => {
            setItems(_array);
            let obj = {};
            _array.forEach((item) => {
              obj[item.date] = {marked: true};
              // console.log('item=',item);
            });
            setMarkedItems(obj);
          },
        );
      },
      // null,
      // ()=>{console.log('isRefresh=', isRefresh); isRefresh?setIsRefresh(false):setIsRefresh(true)}
    );
  }, [curDate]);

  useEffect(() => {
    if (!markedItems) {
      fetchData();
    }
  }, [fetchData, markedItems]);

  const onDayClickHandle = (day) => {
    let tmpList = [];
    items.forEach((item) => {
      if (item.date === day.dateString) tmpList.push(item);
    });
    if (tmpList.length) navigation.navigate('List', {selectedDate: day});
    else
      navigation.navigate('Post', {
        selectedDate: day.dateString,
        fetchData: fetchData,
      });
  };

  const onMonthClickHandle = (month) => {
    setCurDate(month.dateString);
    // console.log(month);
  };

  return (
    <SafeAreaView>
      <Text style={styles.text}>Calendar</Text>
      <Calendar
        style={styles.calendar}
        hideExtraDays
        showWeekNumbers
        markedDates={markedItems}
        onDayPress={(day) => onDayClickHandle(day)}
        onMonthChange={(month) => onMonthClickHandle(month)}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  calendar: {
    marginBottom: 10,
  },
  text: {
    textAlign: 'center',
    padding: 10,
    backgroundColor: 'lightgrey',
    fontSize: 16,
  },
});
