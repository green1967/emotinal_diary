import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import PieChartFig from '../pieChart/pieChart';
import LineChart from '../lineChart/lineChart';
import {Button} from 'react-native-elements';
import {SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';

export default function Stats({navigation, route}) {
  const [currentChart, setCurrentChart] = useState(0);
  const [buttonTitle, setButtonTitle] = useState('LineChart');
  let show = null;

  useEffect(() => {
    //      console.log('cueeChart=', currentChart, 'show=', show)
  }, [currentChart]);

  onClickHandle = () => {
    currentChart ? setCurrentChart(0) : setCurrentChart(1);
    currentChart ? setButtonTitle('LineChart') : setButtonTitle('Pie');
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{}}>
        <Button title={buttonTitle} onPress={() => onClickHandle()} />
      </View>
      {currentChart ? (show = <LineChart />) : (show = <PieChartFig />)}
    </SafeAreaView>
  );
}
